# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# enolp <enolp@softastur.org>, 2017-2018
msgid ""
msgstr ""
"Project-Id-Version: accounts service\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-01-17 09:48-0500\n"
"PO-Revision-Date: 2019-02-22 14:19+0000\n"
"Last-Translator: enolp <enolp@softastur.org>\n"
"Language-Team: Asturian (http://www.transifex.com/freedesktop/accountsservice/language/ast/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ast\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../data/org.freedesktop.accounts.policy.in.h:1
msgid "Change your own user data"
msgstr "Camudacia de los tos datos d'usuariu"

#: ../data/org.freedesktop.accounts.policy.in.h:2
msgid "Authentication is required to change your own user data"
msgstr "Ríquese l'autenticación pa camudar los tos datos d'usuairu"

#: ../data/org.freedesktop.accounts.policy.in.h:3
msgid "Manage user accounts"
msgstr "Xestión de cuentes d'usuariu"

#: ../data/org.freedesktop.accounts.policy.in.h:4
msgid "Authentication is required to change user data"
msgstr "Ríquese l'autenticación pa camudar los tos datos d'usuarios"

#: ../data/org.freedesktop.accounts.policy.in.h:5
msgid "Change the login screen configuration"
msgstr "Cambéu de la configuración de la pantalla d'aniciu de sesión"

#: ../data/org.freedesktop.accounts.policy.in.h:6
msgid "Authentication is required to change the login screen configuration"
msgstr "Ríquese l'autenticación pa camudar la configuración de la pantalla d'aniciu de sesión"

#: ../src/main.c:127
msgid "Output version information and exit"
msgstr "Amuesa la información de versión y cola"

#: ../src/main.c:128
msgid "Replace existing instance"
msgstr "Troca la instancia esistente"

#: ../src/main.c:129
msgid "Enable debugging code"
msgstr "Activa'l códigu de depuración"

#: ../src/main.c:149
msgid ""
"Provides D-Bus interfaces for querying and manipulating\n"
"user account information."
msgstr "Forne interfaces D-Bus pa solicitar y manipular la\ninformación de les cuentes d'usuariu."
